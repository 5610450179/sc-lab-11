package Exception5;

import java.util.ArrayList;

public class Refrigerator {
	int size;
	ArrayList<String> things;
	
	public Refrigerator(int size){
		this.size = size;
		things = new ArrayList<String>();
	}
	
	public void put(String stuff)throws FullException{
		if(things.size()>=size) throw new FullException("Full Fridge");
		things.add(stuff);
	}
	
	public String takeOut(String stuff){
		if(!things.contains(stuff))return null;
		things.remove(stuff);
		return stuff;
	}
	
	public String toString(){
		String str = "Stuff: ";
		for(String stuff:things){
			str += stuff+", ";
		}
		return str.substring(0,str.length()-2);
	}

}
