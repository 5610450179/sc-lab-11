package Exception5;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			Refrigerator ref = new Refrigerator(3);
			ref.put("Bottle");
			System.out.println("Put Bottle");
			ref.put("Cake");
			System.out.println("Put Cake");
			ref.put("Pudding");
			System.out.println("Put Pudding");
			String stuff = ref.takeOut("Pudding");
			System.out.println("Takeout "+stuff);
			System.out.println(ref.toString());
			ref.put("Pudding");
			System.out.println("Put Pudding Again");
			System.out.println(ref.toString());
			//error
			ref.put("Cola");
			System.out.println("Put Cola"); 
			
		}catch(FullException e){
			System.err.println(e.getMessage());
		}

	}

}
