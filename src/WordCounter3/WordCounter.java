package WordCounter3;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


public class WordCounter {
	String message;
	HashMap<String,Integer> wordCount;
	
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();
	}
	
	public void count(){
		ArrayList<String> word = new ArrayList<String>();
		int indx = 0;
		//separate the word
		for(int i=0;i<message.length();i++){
			if(i==message.length()-1){
				if(!word.contains(message.substring(indx,message.length()))){
					word.add(message.substring(indx,message.length()));
				}
			}
			else if(message.charAt(i)==' '){
				if(!word.contains(message.substring(indx,i))){
					word.add(message.substring(indx,i));
				}
				indx = i+1;
			}	
		}
		//put each words to HashMap
		for(String str:word){
			int count = (message.length()-message.replace(str, "").length())/str.length(); 
			wordCount.put(str, count);
		}	
	}
	
	public int hasWord(String word){
		if(wordCount.get(word)==null)return 0;
		return wordCount.get(word);
	}

}
